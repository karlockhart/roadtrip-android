package zer0.co.sendlocationtowebservice;

import android.app.LoaderManager;
import android.provider.BaseColumns;

/**
 * Created by kalockhart on 12/14/16.
 */

public final class LocationCacheContract {
    private LocationCacheContract() {}

    public static class Location implements BaseColumns {
        public static final String TABLE_NAME = "location";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_TRACKING_ID = "tracking_id";
        public static final String COLUMN_NAME_ACCESS_CODE = "access_code";
        public static final String COLUMN_NAME_TRACK_NAME = "track_name";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LNG = "lng";
        public static final String COLUMN_NAME_ACCURACY = "accuracy";
        public static final String COLUMN_NAME_ALTITUDE = "altitude";
        public static final String COLUMN_NAME_VELOCITY = "velocity";
        public static final String COLUMN_NAME_BEARING = "bearing";
        public static final String COLUMN_NAME_COMMENT = "comment";
        public static final String COLUMN_NAME_UPLOADED = "uploaded";
    }
}
