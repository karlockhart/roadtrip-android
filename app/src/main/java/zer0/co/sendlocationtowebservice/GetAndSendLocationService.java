package zer0.co.sendlocationtowebservice;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class GetAndSendLocationService extends Service {
    private static final String TAG = "GETANDSENDLOCATION";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;

    private String nextComment = null;

    public final IBinder mBinder = new LocalBinder();

    private ScheduledFuture<?> senderFuture;
    private String trackingId;
    private String accessCode;
    private String trackName;

    public GetAndSendLocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private class ServiceLocationListener implements LocationListener {
        Location mLastLocation;

        public ServiceLocationListener(String provider) {
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            mLastLocation = location;
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }

    ServiceLocationListener[] mLocationListeners = new ServiceLocationListener[]{
            new ServiceLocationListener(LocationManager.GPS_PROVIDER),
            new ServiceLocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public void onCreate() {
        initializeLocationManager();

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,
                    mLocationListeners[1]
            );
        } catch (SecurityException ex) {

        } catch (IllegalArgumentException ex) {

        }

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,
                    mLocationListeners[0]
            );
        } catch (SecurityException ex) {

        } catch (IllegalArgumentException ex) {

        }

    }

    private void initializeLocationManager() {
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(
                    Context.LOCATION_SERVICE
            );
        }
    }

    public class LocalBinder extends Binder {
        GetAndSendLocationService getService() {
            return GetAndSendLocationService.this;
        }
    }

    public Location getLastLocation() {
        return mLocationListeners[0].mLastLocation;
    }

    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    public ScheduledFuture<?> sendLocation(long interval) {
        final Runnable sender = new Runnable() {

            @Override
            public void run() {
                Log.e("KLKL", "KARLLOG" + getLastLocation().toString());
                cacheAndTrigger();
            }
        };

        return (ScheduledFuture<?>) scheduler.scheduleAtFixedRate(
                sender, 0, interval, TimeUnit.SECONDS
        );
    }

    ;

    public void startSender(String trackingId, String accessCode, String trackName) {
        if (!isRunning()) {
            this.trackingId = trackingId;
            this.accessCode = accessCode;
            this.trackName = trackName;
            this.senderFuture = sendLocation(30);
            Log.e("KLKL", "KARLLOG - START SENDER");
        }
    }

    public void stopSender() {
        if (isRunning()) {
            this.senderFuture.cancel(true);
            Log.e("KLKL", "KARLLOG - STOP SENDER");
        }
    }

    public boolean isRunning() {
        return this.senderFuture != null && !senderFuture.isCancelled() && !senderFuture.isDone();
    }

    public void setNextComment(String comment) {
        nextComment = comment;
    }

    public String getNextComment() {
        return nextComment;
    }

    public void cacheAndTrigger() {
        if (getLastLocation().hasAccuracy()) {
            Log.e("KLKL", "KARLLOG: " + trackingId);

            LocationCacheDatabaseHelper mDatabaseHelper = new LocationCacheDatabaseHelper(this);
            SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(LocationCacheContract.Location.COLUMN_NAME_TRACKING_ID, trackingId);
            values.put(LocationCacheContract.Location.COLUMN_NAME_ACCESS_CODE, accessCode);
            values.put(LocationCacheContract.Location.COLUMN_NAME_TRACK_NAME, trackName);
            values.put(LocationCacheContract.Location.COLUMN_NAME_TIMESTAMP, (new Date().getTime() /1000));
            values.put(LocationCacheContract.Location.COLUMN_NAME_LAT, getLastLocation().getLatitude());
            values.put(LocationCacheContract.Location.COLUMN_NAME_LNG, getLastLocation().getLongitude());
            values.put(LocationCacheContract.Location.COLUMN_NAME_ACCURACY, getLastLocation().getAccuracy());
            if (getLastLocation().hasAltitude()) {
                values.put(LocationCacheContract.Location.COLUMN_NAME_ALTITUDE, getLastLocation().getAltitude());
            }
            if (getLastLocation().hasBearing()) {
                values.put(LocationCacheContract.Location.COLUMN_NAME_BEARING, getLastLocation().getBearing());
            }
            if (getLastLocation().hasSpeed()) {
                values.put(LocationCacheContract.Location.COLUMN_NAME_VELOCITY, getLastLocation().getSpeed());
            }
            if (getNextComment() != null) {
                values.put(LocationCacheContract.Location.COLUMN_NAME_COMMENT, getNextComment());
                setNextComment(null);
            }
            values.put(LocationCacheContract.Location.COLUMN_NAME_UPLOADED, false);
            Log.e("KLKL", "KARLLOG " + values.toString());

            long newRowId = db.insert(LocationCacheContract.Location.TABLE_NAME, null, values);
            Log.e("KLKL", "KARLLOG NRI " + newRowId  + " : " + trackingId);

            SendToWebIntentService.startActionSendData(getContext(), null);
        }
    }

    public Context getContext() {
        return this;
    }
}
