package zer0.co.sendlocationtowebservice;

import android.location.Location;

/**
 * Created by kalockhart on 12/15/16.
 */

public class LocationCache {
    private static LocationCache instance;

    private LocationCache() {}

    public static LocationCache getInstance() {
        if (instance == null) {
            instance = new LocationCache();
        }
        return instance;
    }
}
