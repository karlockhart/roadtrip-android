package zer0.co.sendlocationtowebservice;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by kalockhart on 12/14/16.
 */

public class LocationCacheDatabaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "RTLocationCache.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String BOOL_TYPE = " BOOLEAN";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_LOCATIONS =
            "CREATE TABLE " + LocationCacheContract.Location.TABLE_NAME + " (" +
                    LocationCacheContract.Location._ID + " INTEGER PRIMARY KEY," +
                    LocationCacheContract.Location.COLUMN_NAME_TIMESTAMP + INT_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_TRACKING_ID + TEXT_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_ACCESS_CODE + TEXT_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_TRACK_NAME + TEXT_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_LAT + DOUBLE_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_LNG + DOUBLE_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_ACCURACY + INT_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_ALTITUDE + DOUBLE_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_VELOCITY + DOUBLE_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_BEARING + DOUBLE_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_COMMENT + TEXT_TYPE + COMMA_SEP +
                    LocationCacheContract.Location.COLUMN_NAME_UPLOADED + BOOL_TYPE + " )";
    private static final String SQL_DELETE_LOCATIONS =
            "DROP TABLE IF EXISTS " + LocationCacheContract.Location.TABLE_NAME;

    public LocationCacheDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_LOCATIONS);
        Log.e("KLKL", "KARLLOG: " + SQL_CREATE_LOCATIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_LOCATIONS);
        Log.e("KLKL", "KARLLOG: " + SQL_DELETE_LOCATIONS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
