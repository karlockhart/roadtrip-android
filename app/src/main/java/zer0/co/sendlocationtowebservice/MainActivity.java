package zer0.co.sendlocationtowebservice;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private static final String PREFS_NAME = "SendLocationToWebService";
    private String deviceId;
    private GetAndSendLocationService locationService;
    boolean mBound = false;
    private String prefTrackingId = null;
    private String prefAccessCode = null;
    private String prefTrackName = null;
    private boolean warnedAboutCommentDeletion = false;

    private LocationManager manager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TelephonyManager mTelephonyManager
                = (TelephonyManager) getApplicationContext().getSystemService(
                Context.TELEPHONY_SERVICE
        );

        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        deviceId = mTelephonyManager.getDeviceId();
        this.prefTrackingId = settings.getString("TrackingId", "");
        this.prefAccessCode = settings.getString("AccessCode", "");
        this.prefTrackName = settings.getString("TrackName", "");

        final EditText trackingId = (EditText) findViewById(R.id.trackingIdText);
        trackingId.setText(prefTrackingId);

        final EditText accessCode = (EditText) findViewById(R.id.accessCodeText);
        accessCode.setText(prefAccessCode);

        final EditText trackName = (EditText) findViewById(R.id.trackNameText);
        accessCode.setText(prefTrackName);

        trackingId.setText(deviceId);
        ToggleButton toggleServiceButton = (ToggleButton) findViewById(R.id.toggleServiceButton);
        toggleServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBound) {
                    if (locationService.isRunning()) {
                        locationService.stopSender();
                    } else {
                        if (
                                trackingId.getText().length() == 0 ||
                                        accessCode.getText().length() == 0 ||
                                        trackName.getText().length() == 0

                                ) {
                            CharSequence errorMessage = "Please Tracking ID, Access Code and Track Name.";
                            int duration = Toast.LENGTH_SHORT;
                            Toast errorToast = Toast.makeText(getApplicationContext(), errorMessage, duration);
                            errorToast.show();
                        } else {

                            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                buildAlertMessageNoGps();
                            } else {

                                Log.e("KLKL", "KARRLOG: " + deviceId + " ");

                                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("TrackingId", prefTrackingId);
                                editor.putString("AccessCode", prefAccessCode);
                                editor.putString("TrackName", prefTrackName);
                                editor.commit();

                                locationService.startSender(
                                        trackingId.getText().toString(),
                                        accessCode.getText().toString(),
                                        trackName.getText().toString()
                                );
                            }
                        }
                    }
                    ToggleButton toggleServiceButton = (ToggleButton) findViewById(R.id.toggleServiceButton);
                    toggleServiceButton.setChecked(locationService.isRunning());
                }

            }
        });

        Button sendCommentButton = (Button) findViewById(R.id.sendCommentButton);
        sendCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBound) {
                    EditText commentText = (EditText) findViewById(R.id.locationCommentText);

                    String comment = commentText.getText().toString();
                    if (comment.length() == 0) {
                        comment = null;
                    }

                    if (locationService.getNextComment() != null && !warnedAboutCommentDeletion) {
                        if (!warnedAboutCommentDeletion) {
                            CharSequence warningMessage = "Last comment is still waiting, click again to overwrite";
                            int duration = Toast.LENGTH_SHORT;
                            Toast warnToast = Toast.makeText(getApplicationContext(), warningMessage, duration);
                            warnedAboutCommentDeletion = true;
                            warnToast.show();
                            return;
                        }
                    }
                    warnedAboutCommentDeletion = false;
                    locationService.setNextComment(commentText.getText().toString());
                    commentText.setText("");
                } else {
                    CharSequence errorMessage = "An error occurred binding to the service.";
                    int duration = Toast.LENGTH_SHORT;
                    Toast errorToast = Toast.makeText(getApplicationContext(), errorMessage, duration);
                    errorToast.show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, GetAndSendLocationService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("TrackingId", this.prefTrackingId);
        editor.putString("AccessCode", this.prefAccessCode);
        editor.putString("TrackName", this.prefTrackName);
        editor.commit();

    }


    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            GetAndSendLocationService.LocalBinder mBinder = (GetAndSendLocationService.LocalBinder) iBinder;
            locationService = mBinder.getService();
            mBound = true;
            if (mBound) {
                ToggleButton toggleServiceButton = (ToggleButton) findViewById(R.id.toggleServiceButton);
                toggleServiceButton.setChecked(locationService.isRunning());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
        }
    };

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This application requires GPS, would you like to enable it now?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
