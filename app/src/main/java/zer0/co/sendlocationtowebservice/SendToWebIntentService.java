package zer0.co.sendlocationtowebservice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SendToWebIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_SEND_DATA
            = "zer0.co.sendlocationtowebservice.action.SEND_DATA";

    // TODO: Rename parameters
    private static final String LOCATION = "zer0.co.sendlocationtowebservice.extra.LOCATION";
    private LocationCacheDatabaseHelper mDbHelper;

    public SendToWebIntentService() {
        super("SendToWebIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startSendToWeb(Context context, Location location) {
        Intent intent = new Intent(context, SendToWebIntentService.class);
        intent.setAction(ACTION_SEND_DATA);
        intent.putExtra(LOCATION, location);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionSendData(Context context, Location location) {
        Intent intent = new Intent(context, SendToWebIntentService.class);
        intent.setAction(ACTION_SEND_DATA);
        intent.putExtra(LOCATION, location);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SEND_DATA.equals(action)) {
                final Location location = (Location) intent.getSerializableExtra(LOCATION);
                handleActionSendData(location);
            }
        }
    }

    private JSONObject getLocations() {
        mDbHelper = new LocationCacheDatabaseHelper(this);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                LocationCacheContract.Location._ID,
                LocationCacheContract.Location.COLUMN_NAME_TIMESTAMP,
                LocationCacheContract.Location.COLUMN_NAME_TRACKING_ID,
                LocationCacheContract.Location.COLUMN_NAME_ACCESS_CODE,
                LocationCacheContract.Location.COLUMN_NAME_TRACK_NAME,
                LocationCacheContract.Location.COLUMN_NAME_LAT,
                LocationCacheContract.Location.COLUMN_NAME_LNG,
                LocationCacheContract.Location.COLUMN_NAME_ACCURACY,
                LocationCacheContract.Location.COLUMN_NAME_ALTITUDE,
                LocationCacheContract.Location.COLUMN_NAME_VELOCITY,
                LocationCacheContract.Location.COLUMN_NAME_BEARING,
                LocationCacheContract.Location.COLUMN_NAME_COMMENT
        };

        String selection = LocationCacheContract.Location.COLUMN_NAME_UPLOADED + " = ?";
        String[] selectionArgs = {"0"};
        String sortOrder = LocationCacheContract.Location.COLUMN_NAME_TIMESTAMP + " ASC";

        Cursor c = db.query(
                LocationCacheContract.Location.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder,
                "512"
        );

        c.moveToFirst();
        JSONObject body = new JSONObject();
        JSONArray locations = new JSONArray();

        do {
            JSONObject location = new JSONObject();
            try {
                location.put("_id", c.getString(0));
                location.put("timestamp", c.getString(1));
                location.put("tracking_id", c.getString(2));
                location.put("access_code", c.getString(3));
                location.put("track_name", c.getString(4));
                location.put("lat", c.getString(5));
                location.put("lng", c.getString(6));
                location.put("accuracy", c.getString(7));
                location.put("altitude", c.getString(8));
                location.put("velocity", c.getString(9));
                location.put("bearing", c.getString(10));
                location.put("comment", c.getString(11));
                locations.put(location);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            c.moveToNext();

        } while (!c.isAfterLast());

        try {
            body.put("locations", locations);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        c.close();

        return body;
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSendData(Location location) {
        Log.e("KLKL", "KARLLOG in Location ");
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL("http://auto.coderzer0h.com:3333/locations");
            JSONObject locations = getLocations();
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            urlConnection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            urlConnection.setFixedLengthStreamingMode(locations.toString().getBytes().length);

            urlConnection.connect();

            Log.e("KLKL", "JSON: " + locations.toString());
            OutputStream outs = new BufferedOutputStream(urlConnection.getOutputStream());
            outs.write(locations.toString().getBytes());
            outs.flush();
            outs.close();

            int statusCode = urlConnection.getResponseCode();
            Log.e("KLKL", "Wrote Web " + statusCode + " " + urlConnection.getResponseMessage());
            if (statusCode == urlConnection.HTTP_OK || statusCode == urlConnection.HTTP_ACCEPTED) {
                flushCache(locations);
            }

        } catch (MalformedURLException e) {
            Log.e("KLKL", "KARLLOG " + e.getMessage());
        } catch (IOException e) {
            Log.e("KLKL", "KARLLOG " + e.getMessage());
        } finally {
            Log.e("KLKL", "KARLLOG in Disconnect");
            urlConnection.disconnect();
        }

    }

    private void flushCache(JSONObject locations) {
        mDbHelper = new LocationCacheDatabaseHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        JSONArray uploadedLocations;

        try {
            uploadedLocations = locations.getJSONArray("locations");

            if (locations.length() == 0) {
                return;
            }

            ArrayList<String> ids = new ArrayList<String>();
            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < uploadedLocations.length(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                JSONObject location = uploadedLocations.getJSONObject(i);
                String id = location.getString("_id");
                ids.add(id);
                sb.append("?");
            }

            String selection = LocationCacheContract.Location._ID  + " IN ( " + sb.toString() + ")";
            String[] selectionArgs = ids.toArray(new String[0]);
            db.delete(LocationCacheContract.Location.TABLE_NAME, selection, selectionArgs);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
